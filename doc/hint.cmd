-- load initial data from fixtures
app/console doctrine:fixtures:load

-- run stub producer by this command:
app/console app:stub_generator

-- run worker(consumer) by this command, you can run workers as many as you want:
app/console rabbitmq:consumer -vv -w --route "check" check
app/console rabbitmq:consumer -vv -w --route "dedup" dedup
app/console rabbitmq:consumer -vv -w --route "storage" storage

-- show exchanges 
sudo rabbitmqctl -p /assign list_exchanges

-- show queues
sudo rabbitmqctl -p /assign list_queues
