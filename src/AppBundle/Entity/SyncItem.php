<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="sync_item", indexes={
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SyncItemRepository")
 */

class SyncItem
{
    /**
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="SyncBag", inversedBy="syncItems")
     * @ORM\JoinColumn(name="syncbag_id", referencedColumnName="id", nullable=false)
     **/
    private $syncBag;   
    
    /**
     * @ORM\Column(name="grade", type="string", length=1, nullable=true)
     */
    private $grade;
    
    /**
     * @ORM\OneToOne(targetEntity="SyncData", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="currentdata_id", referencedColumnName="id", nullable=false)
     **/
    private $currentData;

    /**
     * @ORM\ManyToMany(targetEntity="SyncData", cascade={"persist","remove"})
     * @ORM\JoinTable(name="syncitem_has_historydata",
     *      joinColumns={@ORM\JoinColumn(name="syncitem_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="historydata_id", referencedColumnName="id", unique=true)}
     * )
     **/
    private $historyData;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->historyData = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return SyncItem
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set syncBag
     *
     * @param \AppBundle\Entity\SyncBag $syncBag
     * @return SyncItem
     */
    public function setSyncBag(\AppBundle\Entity\SyncBag $syncBag)
    {
        $this->syncBag = $syncBag;

        return $this;
    }

    /**
     * Get syncBag
     *
     * @return \AppBundle\Entity\SyncBag 
     */
    public function getSyncBag()
    {
        return $this->syncBag;
    }

    /**
     * Set currentData
     *
     * @param \AppBundle\Entity\SyncData $currentData
     * @return SyncItem
     */
    public function setCurrentData(\AppBundle\Entity\SyncData $currentData = null)
    {
        $this->currentData = $currentData;

        return $this;
    }

    /**
     * Get currentData
     *
     * @return \AppBundle\Entity\SyncData 
     */
    public function getCurrentData()
    {
        return $this->currentData;
    }

    /**
     * Add historyData
     *
     * @param \AppBundle\Entity\SyncData $historyData
     * @return SyncItem
     */
    public function addHistoryDatum(\AppBundle\Entity\SyncData $historyData)
    {
        $this->historyData[] = $historyData;

        return $this;
    }

    /**
     * Remove historyData
     *
     * @param \AppBundle\Entity\SyncData $historyData
     */
    public function removeHistoryDatum(\AppBundle\Entity\SyncData $historyData)
    {
        $this->historyData->removeElement($historyData);
    }

    /**
     * Get historyData
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoryData()
    {
        return $this->historyData;
    }
}
