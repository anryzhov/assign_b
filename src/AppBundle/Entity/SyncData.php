<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="sync_data", indexes={
 * })
 * @ORM\Entity()
 */

class SyncData
{
    /**
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;
    

    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="data_string0", type="string", length=255, nullable=false)
     */
    protected $dataString0;

    /**
     * @ORM\Column(name="data_string1", type="datetime", nullable=false)
     */
    protected $dataString1;

    /**
     * @ORM\Column(name="data_string2", type="string", length=255, nullable=true)
     */
    protected $dataString2;

    /**
     * @ORM\Column(name="data_string3", type="string", length=16, nullable=false)
     */
    protected $dataString3;

    /**
     * @ORM\Column(name="data_string4", type="string", length=40, nullable=true)
     */
    protected $dataString4;

    /**
     * @ORM\Column(name="data_string5", type="decimal", scale=2, nullable=true)
     */
    protected $dataString5;

    final private function dataString1ToString()
    {
        return $this->dataString1 instanceof \DateTime ? $this->dataString1->format('r') : (string)$this->dataString1;
    }
    
    final public function __toString()
    {
        return $this->dataString0 . '::' . $this->dataString1ToString() . '::' . $this->dataString3;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SyncData
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dataString0
     *
     * @param string $dataString0
     * @return SyncData
     */
    public function setDataString0($dataString0)
    {
        $this->dataString0 = $dataString0;

        return $this;
    }

    /**
     * Get dataString0
     *
     * @return string 
     */
    public function getDataString0()
    {
        return $this->dataString0;
    }

    /**
     * Set dataString1
     *
     * @param \DateTime $dataString1
     * @return SyncData
     */
    public function setDataString1($dataString1)
    {
        $this->dataString1 = $dataString1;

        return $this;
    }

    /**
     * Get dataString1
     *
     * @return \DateTime 
     */
    public function getDataString1()
    {
        return $this->dataString1;
    }

    /**
     * Set dataString2
     *
     * @param string $dataString2
     * @return SyncData
     */
    public function setDataString2($dataString2)
    {
        $this->dataString2 = $dataString2;

        return $this;
    }

    /**
     * Get dataString2
     *
     * @return string 
     */
    public function getDataString2()
    {
        return $this->dataString2;
    }

    /**
     * Set dataString3
     *
     * @param string $dataString3
     * @return SyncData
     */
    public function setDataString3($dataString3)
    {
        $this->dataString3 = $dataString3;

        return $this;
    }

    /**
     * Get dataString3
     *
     * @return string 
     */
    public function getDataString3()
    {
        return $this->dataString3;
    }

    /**
     * Set dataString4
     *
     * @param string $dataString4
     * @return SyncData
     */
    public function setDataString4($dataString4)
    {
        $this->dataString4 = $dataString4;

        return $this;
    }

    /**
     * Get dataString4
     *
     * @return string 
     */
    public function getDataString4()
    {
        return $this->dataString4;
    }

    /**
     * Set dataString5
     *
     * @param string $dataString5
     * @return SyncData
     */
    public function setDataString5($dataString5)
    {
        $this->dataString5 = $dataString5;

        return $this;
    }

    /**
     * Get dataString5
     *
     * @return string 
     */
    public function getDataString5()
    {
        return $this->dataString5;
    }
}
