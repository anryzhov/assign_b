<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="sync_bag", indexes={
 * })
 * @ORM\Entity()
 */

class SyncBag
{
    /**
	 * @ORM\Column(name="id", type="guid")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 */
	private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="MappedSource")
     * @ORM\JoinColumn(name="source_id", referencedColumnName="id", nullable=false)
     **/
    private $source;
    
    /**
     * @ORM\OneToMany(targetEntity="SyncItem", mappedBy="syncBag")
     **/
    private $syncItems;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->syncItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\MappedSource $source
     * @return SyncBag
     */
    public function setSource(\AppBundle\Entity\MappedSource $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\MappedSource 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Add syncItems
     *
     * @param \AppBundle\Entity\SyncItem $syncItems
     * @return SyncBag
     */
    public function addSyncItem(\AppBundle\Entity\SyncItem $syncItems)
    {
        $this->syncItems[] = $syncItems;

        return $this;
    }

    /**
     * Remove syncItems
     *
     * @param \AppBundle\Entity\SyncItem $syncItems
     */
    public function removeSyncItem(\AppBundle\Entity\SyncItem $syncItems)
    {
        $this->syncItems->removeElement($syncItems);
    }

    /**
     * Get syncItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSyncItems()
    {
        return $this->syncItems;
    }

}
