<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class PostalCode extends Constraint
{
    public $message = "four_digit_two_letter_format";
}