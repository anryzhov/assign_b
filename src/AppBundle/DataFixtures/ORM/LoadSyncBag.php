<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\AbstractFixture;
use AppBundle\Entity\SyncBag;


class LoadSyncBag extends AbstractFixture
{

    public function load(ObjectManager $manager)
    {
        
        $item = new SyncBag();
        $item
                ->setSource($this->getReference('assign_test_source'))
        ;
        
        $manager->persist($item);
        $manager->flush();
        
        $this->output->writeln('sync_bag[0]: "' . $item->getId() . '"');
    

    }// load

    public function getOrder()
    {
        return 20;
    }
}