<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\DataFixtures\AbstractFixture;
use AppBundle\Entity\MappedSource;


class LoadMappedSource extends AbstractFixture
{

    public function load(ObjectManager $manager)
    {
            $item = new MappedSource();
            $item
                    ->setUrl('assign_test_source')
            ;
            
            $manager->persist($item);
            $manager->flush();

            $reference = 'assign_test_source';
            
            $this->setReference($reference, $item);
            
            $this->output->writeln('mapped_source[0]: "' . $reference . '"');

    }// load

    public function getOrder()
    {
        return 10;
    }
}