<?php

namespace AppBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture as BaseAbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

abstract class AbstractFixture extends BaseAbstractFixture implements OrderedFixtureInterface
{
    protected $output;

    public function __construct()
    {
        $this->output = new ConsoleOutput();
    }

}