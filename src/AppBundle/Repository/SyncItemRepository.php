<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\SyncData;


class SyncItemRepository extends EntityRepository
{
    
    public function findDuplicat(SyncData $data)
    {
        $qb = $this->createQueryBuilder('item')
            ->select(array(
                'item','current'
            ))
			->innerJoin('item.currentData', 'current')
        ;
        
        $whereConditions[] = $qb->expr()->eq('current.dataString0', ':ds0');
        $whereConditions[] = $qb->expr()->eq('current.dataString3', ':ds3');
        $whereConditions[] = $qb->expr()->eq('current.dataString5', ':ds5');
        
        $parameters['ds0'] = $data->getDataString0();
        $parameters['ds3'] = $data->getDataString3();
        $parameters['ds5'] = $data->getDataString5();
		
        $qb->add('where', call_user_func_array(array($qb->expr(), 'andx'), $whereConditions));
        $qb->setParameters($parameters);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findContainer(SyncData $data)
    {
        $qb = $this->createQueryBuilder('item')
            ->select(array(
                'item'
            ))
			->innerJoin('item.currentData', 'current')
        ;
        
        $whereConditions[] = $qb->expr()->eq('current.dataString0', ':ds0');
        $whereConditions[] = $qb->expr()->eq('current.dataString3', ':ds3');
        
        $parameters['ds0'] = $data->getDataString0();
        $parameters['ds3'] = $data->getDataString3();
		
        $qb->add('where', call_user_func_array(array($qb->expr(), 'andx'), $whereConditions));
        $qb->setParameters($parameters);
        
        return $qb->getQuery()->getSingleResult();
    }

}
