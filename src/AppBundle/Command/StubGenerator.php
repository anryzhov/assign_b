<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Psr\Log\LoggerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use JMS\Serializer\Serializer;

use AppBundle\Entity\SyncData;
use AppBundle\Service\MessageRoute;

class StubGenerator extends ContainerAwareCommand implements MessageRoute
{
    private $logger;
    private $producer;
    private $serializer;

    private $urlBag = ['http://a.site.com/file1.html','http://b.site.com/file2.xml','http://c.site.com/file3.txt'];
    private $postalBag = ['1234AB', '2345BC', '3456CD', '4567DE', '5678EF', '6789FG'];
    private $categoryBag = ['category_A','category_B','category_C','category_D','category_E','category_F',];
    
    public function __construct(LoggerInterface $logger, Producer $producer, Serializer $serializer)
    {
        $this->logger = $logger;
        $this->producer = $producer;
        $this->serializer = $serializer;

        parent::__construct();
    }

    public function stopGenerator($signo)
    {
        $this->logger->debug("-- stub generator stopped by signal: " . $signo . " --");
        exit();
    }


    protected function configure()
    {
        parent::configure();
        
        $this
            ->setName('app:stub_generator')
            ->addOption('messages', 'm', InputOption::VALUE_OPTIONAL, 'Messages to generate', 0)
            ->addOption('with-signals', 'w', InputOption::VALUE_NONE, 'Enable catching of system signals')
            ->setDescription('generate stub message to test process worker')
        ;
    }

    final private function getRandomItem(array $bag)
    {
        return $bag[rand(0, count($bag)-1)];
    }
    
    final private function getRandomDate()
    {
        $date = new \DateTime();
        return $date->sub(new \DateInterval('P' . rand(0,50000) . 'D'));
    }
    
    final private function doMessage()
    {
        $data = new SyncData();

        $data
                ->setDataString0($this->getRandomItem($this->urlBag))
                ->setDataString1($this->getRandomDate())
                ->setDataString2($this->getRandomItem($this->categoryBag))
                ->setDataString3($this->getRandomItem($this->postalBag))
                ->setDataString4(md5(rand()))
                ->setDataString5(rand(10000,99999))
        ;
        
        return $this->serializer->serialize($data, 'json');

    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->debug("-- stub generator started --");
        
        if ($input->getOption('with-signals') && extension_loaded('pcntl')) {
            
            if (!function_exists('pcntl_signal')) {
                throw new \BadFunctionCallException("Function 'pcntl_signal' is referenced in the php.ini 'disable_functions' and can't be called.");
            }
            
            $this->logger->debug("-- setup signal handler --");

            pcntl_signal(SIGTERM, array(&$this, 'stopGenerator'));
            pcntl_signal(SIGINT, array(&$this, 'stopGenerator'));
            pcntl_signal(SIGHUP, array(&$this, 'stopGenerator'));
        }

        $i = 0; 
        
        while (true) {
            
            sleep(rand(5, 10));
            
            if ($input->getOption('with-signals') && extension_loaded('pcntl')) {
                pcntl_signal_dispatch();
            }
            
            $message = $this->doMessage();
            
            try {

                $this->producer->produce($message, self::CHECK_MSG_ROUTE);
                $this->logger->debug('message published: ' . $message);
                
            } catch (\Exception $ex) {
                $this->logger->debug('exception while publish message: "' . $ex->getMessage() . '"');
            }
            
            ++$i;

            if ($input->getOption('messages') > 0 && $input->getOption('messages') <= $i) {
                break; 
            }
        }
    }
}