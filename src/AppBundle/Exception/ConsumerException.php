<?php

namespace AppBundle\Exception;

class ConsumerException extends \Exception implements ExceptionInterface
{
    private $status;

    final public function setStatus($status)
    {
        $this->status = $status;
    }

    final public function getStatus()
    {
        return $this->status;
    }

}
