<?php

namespace AppBundle\Exception;

interface ExceptionInterface
{
    public function setStatus($status);
    public function getStatus();
}
