<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Service;

use Symfony\Component\Validator\ValidatorInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

use AppBundle\Entity\SyncData;
use AppBundle\Exception\ConsumerException;


class CheckConsumer extends AbstractConsumer
{
    
    private $producer;
    private $validator;

    final protected function process(SyncData $data)
    {
        $status = self::MSG_ACK;
        
        try {
            
            if (count($this->validator->validate($data, ['grade_f'])) > 0) {
               $ex = new ConsumerException('Reject invalid data');
               $ex->setStatus(self::MSG_REJECT);
               throw $ex;
            }
            
            $this->producer->produce($this->serializer->serialize($data, 'json'), self::DEDUP_MSG_ROUTE);
            $this->logger->debug('data validated and requeued: "' . $data .'"');
            
        } catch (ConsumerException $ex) {
            
            $status = $ex->getStatus();
            $this->logger->debug($ex->getMessage());
            
        }

        return $status;
        
    }

    final public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
    
    final public function setProducer(Producer $producer)
    {
        $this->producer = $producer;
    }

}