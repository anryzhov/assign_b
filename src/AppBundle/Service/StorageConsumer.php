<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Service;

use Symfony\Component\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

use AppBundle\Entity\SyncData;
use AppBundle\Entity\SyncItem;
use AppBundle\Exception\ConsumerException;

class StorageConsumer extends AbstractConsumer
{
    private $manager;
    private $validator;

    final protected function process(SyncData $data)
    {
        $status = self::MSG_ACK;
        $grade = $this->doGrade($data);
        
        try {

            $item = $this->manager->getRepository('AppBundle:SyncItem')->findContainer($data);

            if ($item instanceof SyncItem) {
                
                $item
                        ->addHistoryDatum($item->getCurrentData())
                        ->setCurrentData($data)
                        ->getGrade($grade)
                ;
                
                $this->manager->persist($item);
                $this->manager->flush();
                $this->logger->debug('sync_item updated with data: "' . $data . '"');
            }
            
            
        } catch (ConsumerException $ex) {
            
            $status = $ex->getStatus();
            $this->logger->debug($ex->getMessage());
            
        } catch (NonUniqueResultException $ex) {
            
            $status = self::MSG_REJECT;
            $this->logger->debug('Found more than one master sync_item: "' . $data . '"');
            
        } catch (NoResultException $ex) {
            
            $source = $this->manager->getRepository('AppBundle:MappedSource')->findOneByUrl('assign_test_source');
            $bag = $this->manager->getRepository('AppBundle:SyncBag')->findOneBySource($source);
            
            $item = new SyncItem();
            $item
                    ->setCurrentData($data)
                    ->setGrade($grade)
                    ->setSyncBag($bag)
            ;
            
            $this->manager->persist($item);
            $this->manager->flush();
            $this->logger->debug('new sync_item flushed with data: "' . $data . '"');
            
        }
            
        return $status;
        
    }

    final public function setManager(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
    
    final public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
    
    final private function doGrade(SyncData $data) {

        $result = 'A';
        $grades = [ 'D' => 'grade_d',  'E' => 'grade_e',  'C' => 'grade_c',  'B' => 'grade_b',  'A' => 'grade_a' ];

        foreach ($grades as $key => $value) {
            if (count($this->validator->validate($data, [$value])) > 0) {
                $result = $key;
                break;
            }
        }

        return $result;

    }
}