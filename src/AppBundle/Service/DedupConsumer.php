<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

use AppBundle\Entity\SyncData;
use AppBundle\Exception\ConsumerException;


class DedupConsumer extends AbstractConsumer
{

    private $manager;
    private $producer;

    final protected function process(SyncData $data)
    {
        $status = self::MSG_ACK;
        
        try {
            $result = $this->manager->getRepository('AppBundle:SyncItem')->findDuplicat($data);

            if (count($result) > 0) {
               $ex = new ConsumerException('Reject duplication data');
               $ex->setStatus(self::MSG_REJECT);
               throw $ex;
            }
            
            $this->producer->produce($this->serializer->serialize($data, 'json'), self::STORAGE_MSG_ROUTE);
            $this->logger->debug('data deduplicated and requeued: "' . $data .'"');
            
        } catch (ConsumerException $ex) {
            
            $status = $ex->getStatus();
            $this->logger->debug($ex->getMessage());
            
        }

        return $status;
    }

    final public function setManager(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
    
    final public function setProducer(Producer $producer)
    {
        $this->producer = $producer;
    }

}