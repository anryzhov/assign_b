<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Service;


interface MessageRoute 
{
    const CHECK_MSG_ROUTE = 'check';
    const DEDUP_MSG_ROUTE = 'dedup';
    const STORAGE_MSG_ROUTE = 'storage';
}