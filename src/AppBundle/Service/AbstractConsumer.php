<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Service;


use Psr\Log\LoggerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use JMS\Serializer\Serializer;

use AppBundle\Entity\SyncData;

abstract class AbstractConsumer implements ConsumerInterface, MessageRoute
{
    protected $logger;
    protected $serializer;

    public function __construct(LoggerInterface $logger, Serializer $serializer)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    abstract protected function process(SyncData $data);


    public function execute(AMQPMessage $msg)
    {
        
        $this->logger->debug('consumer receive: "' . $msg->body .'"');


        try {
            $data = $this->serializer->deserialize($msg->body, 'AppBundle\Entity\SyncData', 'json');

            $success = $this->process($data);

        } catch (\Exception $ex) {
            
            $success = self::MSG_SINGLE_NACK_REQUEUE;
            $this->logger->debug('requeue message while exception: "' . $ex->getMessage().'"');
        }

        return $success;
    }
}