<?php

/**
 * @author Aleksandr N. Ryzhov <a.n.ryzhov@gmail.com>
 */

namespace AppBundle\Service;

use OldSound\RabbitMqBundle\RabbitMq\Producer;

class CommonProducer extends Producer
{
    
    public function produce($msgBody, $routingKey)
    {
        $this->setContentType('application/json')->publish($msgBody, $routingKey);
    }
            
}